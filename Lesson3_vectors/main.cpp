#include <iostream>
#include "Vector.h"

void additional_test();
void basic_test();
void push_pop_test();
void operators_and_copy_builder();

int main()
{
	//basic_test();
	//additional_test();
	//push_pop_test();
	operators_and_copy_builder();
	return 0;
}

void additional_test()
{
	Vector v(10);
	v.reserve(12);
	v.assign(0);
	v.printVector();
	std::cout << "\n" << std::endl;
	v.resize(10);
	v.printVector();
	std::cout << "\n" << std::endl;
	v.resize(12, 5);
	v.printVector();	
}

void basic_test()
{
	Vector v1(1);
	Vector v2(10);
	//basic test for V1
	std::cout << "V1 size: " << v1.size() << "\n" << std::endl;
	std::cout << "V1 capacity: " << v1.capacity() << "\n" << std::endl;
	std::cout << "V1 resizeFactor: " << v1.resizeFactor() << "\n" << std::endl;
	std::cout << "V1 empty	(0 - false, 1 - true): " << v1.empty() << "\n" << std::endl;

	//basic test for V2
	std::cout << "V2 size: " << v2.size() << "\n" << std::endl;
	std::cout << "V2 capacity: " << v2.capacity() << "\n" << std::endl;
	std::cout << "V2 resizeFactor: " << v2.resizeFactor() << "\n" << std::endl;
	std::cout << "V2 empty (0 - false, 1 - true): " << v2.empty() << "\n" << std::endl;
}

void push_pop_test()
{
	Vector v1(10);
	for (int i = 0; i < 10 ; i++)
	{
		v1.push_back(i);
	}
	for (int i = 0; i < v1.capacity(); i++)
	{
		std::cout << v1.pop_back() << std::endl;
	}
	v1.pop_back();
}

void operators_and_copy_builder()
{
	Vector v1(1);
	Vector v2(10);
	v2.assign(0);
	v1 = v2;
	v1.printVector();
	Vector v3(v1);
	std::cout << "\n" << std::endl;
	v3.printVector();
	Vector v4(10);
	for (int i = 0; i < v4.capacity(); i++)
	{
		v4.push_back(i);
	}
	std::cout << v4[2] << std::endl;
}