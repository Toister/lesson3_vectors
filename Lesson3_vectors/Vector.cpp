#include <iostream>
#include "Vector.h"

Vector::Vector(int n)
{
	if (n < 2)
	{
		n = 2;
	}
	_elements = new int[n];
	_capacity = n;
	_size = 0;
	_resizeFactor = n;
}

Vector::Vector(const Vector& other)
{
	*this = other;
}

Vector::~Vector()
{
	delete[] _elements;
}

int Vector::size() const
{
	//return size of vector
	return _size;
}

int Vector::capacity() const
{
	//return capacity of vector
	return _capacity;
}
int Vector::resizeFactor() const
{
	//return vector's resizeFactor
	return _resizeFactor;
}
bool Vector::empty() const
{
	//returns true if size = 0
	return _size == 0;
}

void Vector::push_back(const int& val)
{
	//adds element at the end of the vector
	if (_size == _capacity)
	{
		reserve(_capacity);
	}
	_elements[_size++] = val;
}

int Vector::pop_back()
{
	//removes and returns the last element of the vector
	if (!empty())
	{
		return _elements[--_size];
	}
	std::cerr << "error: pop from empty vector" << std::endl;
	return -9999;
}
//TODO: understand what the fuction suppose to do
void Vector::reserve(int n)
{
	//change the capacity
	bool flag = true;
	if (_capacity <= n)
	{
		int counter = 1;
		while (flag)
		{
			if (counter * _resizeFactor + _capacity > n)
			{
				int n = _capacity + (_resizeFactor * counter);
				cpp_realloc(n);
				_capacity +=  _resizeFactor * counter;
				flag = false;
			}
			else
			{
				counter += 1;
			}
		}
	}
}

//TODO: understand what the fuction suppose to do
void Vector::resize(int n)
{
	//change _size to n, unless n is greater than the vector's capacity
	if (_capacity != n)
	{
		if (_capacity < n)
		{
			reserve(n);
		}
		else
		{
			cpp_realloc(n);
			_capacity = n;
			if (_size > n)
			{
				_size = n;
			}
		}
	}
}

void Vector::resize(int n, const int& val)
{
	//same as above, if new elements added their value is val
	int flag = false;
	if (_capacity < n)
	{
		flag = true;
	}
	int original_size = _size;
	resize(n);
	if (flag)
	{
		for(int i = original_size; i < _capacity; i++)
		{
			_elements[i] = val;
		}
	}
}

void Vector::assign(int val)
{
	//assigns val to all elemnts
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = val;
	}
	_size = _capacity;
}

void Vector::printVector() const
{
	for (int i = 0; i < _capacity; i++) 
	{
		std::cout << _elements[i] << " " << std::endl;	
	}
}

Vector& Vector::operator=(const Vector& other)
{
	_size = other.size();
	_capacity = other.capacity();
	_resizeFactor = other.resizeFactor();
	delete[] _elements;
	_elements = new int[other.capacity()];
	for (int i = 0; i < _capacity; i++)
	{
		_elements[i] = other._elements[i];
	}
	return *this;
}

int& Vector::operator[](int n) const
{
	//n'th element
	if (n > _size || n < 0)
	{
		std::cerr << "Out of bounds";
		return _elements[0];
	}
	return _elements[n];
}

int Vector::size_of_vector()
{
	return sizeof(*_elements);
}

void Vector::cpp_realloc(int n)
{
	int* temp = new int[_capacity];
	//copy the old array to the new one
	for (int i = 0; i < _size; i++)
	{
		temp[i] = _elements[i];
	}
	//clears the old _elements
	Vector::~Vector();
	//creates the _elements with new size and the new Vector
	_elements = new int[n];
	if (_size > n)
	{
		_size = n;
	}
	//copies back the valuse to the array
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = temp[i];
	}
	delete[] temp;
}